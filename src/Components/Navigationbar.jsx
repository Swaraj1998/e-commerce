import "./Navigationbar.css";
import { NavLink } from "react-router-dom";

export default function Navigationbar() {
  return (
    <div className="sidebar">
      <div className="sidebarWrapper">
        <ul className="sidebarList">
          <li className="sidebarListItem">
            <div className="sidebarwrap">
              <NavLink exact activeclassName="sidebarLink" to="/electronics">
                <img
                  className="sidebarimg"
                  src="https://images.unsplash.com/photo-1426024084828-5da21e13f5dc?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=753&q=80"
                  alt="here lies img"
                />
              </NavLink>
              <span className="sidebarListItemText">Electronics</span>
            </div>
          </li>
          <li className="sidebarListItem">
            <div className="sidebarwrap">
              <NavLink exact activeclassName="sidebarLink" to="/sports">
                <img
                  className="sidebarimg"
                  src="https://etimg.etb2bimg.com/photo/74881928.cms"
                  alt="here lies img"
                />
              </NavLink>
              <span className="sidebarListItemText">Sports</span>
            </div>
          </li>
          <li className="sidebarListItem">
            <div className="sidebarwrap">
              <NavLink exact activeclassName="sidebarLink" to="/books">
                <img
                  className="sidebarimg"
                  src="https://d2r68eeixpqexd.cloudfront.net/41fd2ced63aa8d47a3142fa4cd46849b.jpg"
                  alt="here lies img"
                />
              </NavLink>
              <span className="sidebarListItemText">Books</span>
            </div>
          </li>
          <li className="sidebarListItem">
            <div className="sidebarwrap">
              <img
                className="sidebarimg"
                src="https://www.thoughtco.com/thmb/bMpVlJO3cIbaDNa-z9WuOk7oFRo=/768x0/filters:no_upscale():max_bytes(150000):strip_icc():format(webp)/close-up-of-clothes-hanging-in-row-739240657-5a78b11f8e1b6e003715c0ec.jpg"
                alt="here lies img"
              />
              <span className="sidebarListItemText">Clothing</span>
            </div>
          </li>
          <li className="sidebarListItem">
            <div className="sidebarwrap">
              <img
                className="sidebarimg"
                src="https://sensing.konicaminolta.asia/wp-content/uploads/2018/12/Household-Appliances-testing-application.jpg"
                alt="here lies img"
              />
              <span className="sidebarListItemText">Appliances</span>
            </div>
          </li>
          <li className="sidebarListItem">
            <div className="sidebarwrap">
              <img
                className="sidebarimg"
                src="https://ichef.bbci.co.uk/news/976/cpsprodpb/13729/production/_112375697_1331db7a-17c0-4401-8cac-6a2309ff49b6.jpg"
                alt="here lies img"
              />
              <span className="sidebarListItemText">Games</span>
            </div>
          </li>
          <li className="sidebarListItem">
            <div className="sidebarwrap">
              <img
                className="sidebarimg"
                src="https://www.supermarketnews.com/sites/supermarketnews.com/files/styles/article_featured_standard/public/AmazonFresh-groceries-delivery.png?itok=quqE0yH_"
                alt="here lies img"
              />
              <span className="sidebarListItemText">Grocery</span>
            </div>
          </li>
        </ul>
      </div>
    </div>
  );
}
