import "./Topbar.css";
import { NavLink } from "react-router-dom";
import SearchIcon from "@material-ui/icons/Search";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { logOut } from "../Actions";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";

export default function Topbar() {
  const dispatch = useDispatch();
  const list = useSelector((state) => state.mainReducer.list);
  const list2 = useSelector((state) => state.cartReducer.list);
  return (
    <div className="TopbarContainer">
      <div className="TopbarLeft">
        <span className="logo">Jaraws</span>
      </div>
      <div className="TopbarCenter">
        <div className="searchbar">
          <SearchIcon className="searchIcon" />
          <input placeholder="Search Jaraws" className="searchInput" />
        </div>
      </div>
      <div className="TopbarRight">
        <div className="TopbarLinks">
          <NavLink exact activeclassName="TopbarLink" to="/home">
            Home Page
          </NavLink>
        </div>
        <div className="TopbarIcons">
          <div className="TopbarIconItem"></div>
          <div className="TopbarIconItem">
            <NavLink exact activeclassName="TopbarLink" to="/cart">
              <ShoppingCartIcon />
            </NavLink>

            <span className="topBarIconBadge">{list2.length}</span>
          </div>
          <div className="TopbarIconItem"></div>
        </div>
        <div>
          {list.map((abc) => {
            return <img src={abc.profileImg} alt="" className="TopbarImg" />;
          })}
        </div>
        <NavLink
          exact
          activeclassName="TopbarLink"
          to="/login"
          onClick={() => dispatch(logOut())}
        >
          <ExitToAppIcon />
        </NavLink>
      </div>
    </div>
  );
}
