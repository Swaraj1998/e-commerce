import React from "react";
import "./Product.css";
import { useDispatch } from "react-redux";
import { addToCart } from "../Actions";
import { removeFromCart } from "../Actions";
import { useState } from "react";

function Product(props) {
  const dispatch = useDispatch();
  const [isShown, setIsShown] = useState(false);
  return (
    <>
      <div className="products">
        <div
          className="product"
          onMouseEnter={() => setIsShown(true)}
          onMouseLeave={() => setIsShown(false)}
        >
          <img src={props.img} alt="pic" className="product__img" />
          <div className="product__info">
            <span className="product__category"> {props.name}</span>
            {isShown && (
              <div>
                <b>Details:</b>
                {props.details}
              </div>
            )}
            <h3 className="product__title">₹{props.price}</h3>

            <button
              className="addToCartBtn"
              onClick={() =>
                dispatch(
                  addToCart(props.id, props.name, props.price, props.img)
                )
              }
            >
              add to cart
            </button>
          </div>
        </div>
      </div>
    </>
  );
}

export default Product;
