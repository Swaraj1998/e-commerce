import React, { useState } from "react";
import "./SignIn.css";
import { useDispatch } from "react-redux";
import Users from "../Users";
import dataStore from "../Actions/index";
import history from "../Components/history";

const SignIn = () => {
  const dispatch = useDispatch();
  const [email1, setEmail] = useState("");
  const [password1, setPassword] = useState("");

  let [id, setID] = useState(3);

  const handleSubmit = () => {
    for (id = 0; id <= Users.length - 1; id++) {
      if (email1 === Users[id].email && password1 === Users[id].password) {
        console.log("welcome", id, Users[id].username);
        setID(id);

        dispatch(
          dataStore(
            Users[id].id,
            Users[id].username,
            Users[id].profileImg,
            Users[id].age,
            Users[id].email
          )
        );

        break;
      }
    }
    history.push("/home");
  };

  return (
    <div className="login">
      <div
        className="login-form"
        // onSubmit={(e) => handleSubmit(e)}
      >
        <h2>Login Here</h2>
        <input
          type="Email"
          placeholder="Email"
          value={email1}
          onChange={(event) => setEmail(event.target.value)}
        />
        <input
          type="Password"
          placeholder="Password"
          value={password1}
          onChange={(event) => setPassword(event.target.value)}
        />
        <button className="submit-button" onClick={() => handleSubmit()}>
          Login
        </button>
      </div>
      <div></div>
    </div>
  );
};

export default SignIn;

//PopUp code

/* <Popup trigger={buttonPopup} setTrigger={setButtonPopup}>
        <h2>{loginStatus()}</h2>
        <div>Welcome {Users[iid].username}</div>
      </Popup>*/
