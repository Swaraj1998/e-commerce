import { useSelector, useDispatch } from "react-redux";
import { removeFromCart, emptyCart } from "../Actions";
import "./Cart.css";
import Topbar from "../Components/Topbar";

export const Cart = () => {
  const dispatch = useDispatch();
  const list = useSelector((state) => state.cartReducer.list);
  let totalCost = list.map((abc) => abc.price).reduce((a, b) => a + b, 0);

  return (
    <>
      <Topbar />
      <h1>CART</h1>
      {list.map((abc) => {
        return (
          <>
            <div className="eachItem">
              <div className="eachProductItem">
                <div className="productID">ID - {abc.id}</div>
              </div>
              <div className="eachProductItem">
                <div className="productName"> Item Name - {abc.name}</div>
              </div>
              <div className="eachProductItem">
                <img className="productImg" src={abc.image} />
              </div>
              {/* <button className="qtyMinus">-</button>
              <div className="qty">qty</div>
              <button className="qtyPlus">+</button> */}
              <div className="eachProductItem">
                <div className="productPrice">price - ₹{abc.price}</div>
              </div>
              <div className="eachProductItem">
                <button
                  className="deleteBtn"
                  onClick={() => dispatch(removeFromCart(abc.id))}
                >
                  Remove From Cart
                </button>
              </div>
            </div>
          </>
        );
      })}

      <div className="">total price - ₹{totalCost}</div>
      <div>
        <button className="">Proceed To Payment</button>
      </div>
      <button className="emptyCartBtn" onClick={() => dispatch(emptyCart())}>
        Empty Cart
      </button>
    </>
  );
};
