import "./HomePage.css";
import Topbar from "../Components/Topbar";
import Navigationbar from "../Components/Navigationbar";
import Product from "../Components/Product";
import history from "../Components/history";
import { useSelector, useDispatch } from "react-redux";
import { products } from "../products";

export default function HomePage() {
  const list2 = useSelector((state) => state.mainReducer.list);
  if (list2.length === 0) history.push("/");
  return (
    <>
      <Topbar />
      <div className="homeContainer">
        <Navigationbar />
        <div>
          {products.map((val) => {
            return (
              <Product
                img={val.image}
                name={val.name}
                price={val.price}
                id={val.id}
                details={val.detail}
              />
            );
          })}
        </div>
      </div>
    </>
  );
}
