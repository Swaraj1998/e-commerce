export default function dataStore(id, username, profileImg, age, email) {
  return {
    type: "DATA",
    payload: {
      id: id,
      username: username,
      profileImg: profileImg,
      age: age,
      email: email,
    },
  };
}
export const logOut = () => {
  return {
    type: "LOGOUT_RESET",
  };
};

export const addToCart = (id, name, price, image) => {
  return {
    type: "ADD_TO_CART",
    payload: {
      id: id,
      name: name,
      price: price,
      image: image,
    },
  };
};
export const removeFromCart = (id) => {
  return {
    type: "REMOVE_FROM_CART",
    payload: {
      id: id,
    },
  };
};
export const emptyCart = () => {
  return {
    type: "EMPTY_CART",
  };
};

export const qtyMinus = (id) => {
  return {
    type: "QTY_MINUS",
    payload: {
      id: id,
    },
  };
};

export const qtyPlus = (id, name, price, image) => {
  return {
    type: "QTY_PLUS",
    payload: {
      id: id,
      name: name,
      price: price,
      image: image,
    },
  };
};
