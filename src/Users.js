const Users = [
  {
    id: 1,
    username: "arpit",
    password: "12345",
    profileImg:
      "https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
    age: 21,
    email: "arpit@paytm.com",
  },
  {
    id: 2,
    username: "swaraj",
    password: "abcde",
    profileImg:
      "https://images.unsplash.com/flagged/photo-1573740144655-bbb6e88fb18a?ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8dXNlcnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
    age: 22,
    email: "swaraj@paytm.com",
  },
  {
    id: 3,
    username: "pawan",
    password: "1a2b3c",
    profileImg:
      "https://images.unsplash.com/photo-1560250097-0b93528c311a?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Nnx8dXNlcnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
    age: 23,
    email: "pawan@paytm.com",
  },
  {
    id: 4,
    username: "Stranger",
  },
];
export default Users;
