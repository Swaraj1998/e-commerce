export const products = [
  {
    id: 1,
    name: "Jack Reacher - Killing Floor",

    type: "Books",
    detail:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam tristique varius tincidunt. Sed faucibus accumsan lacus. Ut elementum vel ligula sed euismod. Duis laoreet gravida venenatis. Donec sit amet leo a eros fermentum fermentum. Quisque luctus purus massa, eget sollicitudin nulla mollis vitae. Etiam et pharetra dui, quis sagittis urna.",
    price: 349,
    image:
      "https://rukminim1.flixcart.com/image/416/416/jyrl4sw0/book/8/1/5/killing-floor-original-imafe3mrxucu7py4.jpeg",
  },
  {
    id: 2,
    name: "Harry Potter and the Order of the Phoenix",
    type: "Books",
    detail:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam tristique varius tincidunt. Sed faucibus accumsan lacus. Ut elementum vel ligula sed euismod. Duis laoreet gravida venenatis. Donec sit amet leo a eros fermentum fermentum. Quisque luctus purus massa, eget sollicitudin nulla mollis vitae. Etiam et pharetra dui, quis sagittis urna.",
    price: 499,
    image:
      "https://rukminim1.flixcart.com/image/416/416/kq43iq80/book/l/p/h/harry-potter-and-the-order-of-the-phoenix-gryffindor-edition-original-imag46uq5zzhyhhr.jpeg?q=70",
  },
  {
    id: 3,
    name: "ASUS ROG Phone 5 (Black, 128 GB)  (8 GB RAM)",
    type: "Electronics",
    detail:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam tristique varius tincidunt. Sed faucibus accumsan lacus. Ut elementum vel ligula sed euismod. Duis laoreet gravida venenatis. Donec sit amet leo a eros fermentum fermentum. Quisque luctus purus massa, eget sollicitudin nulla mollis vitae. Etiam et pharetra dui, quis sagittis urna.",
    price: 49999,
    image:
      "https://static.digit.in/default/e585860074a3572c1fbb7c2bb92a42f21bf05e44.jpeg",
  },
  {
    id: 4,
    name: "APPLE MacBook Air M1 - (8 GB/256 GB SSD/Mac OS Big Sur) MGN63HN/A",
    type: "Electronics",
    detail:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam tristique varius tincidunt. Sed faucibus accumsan lacus. Ut elementum vel ligula sed euismod. Duis laoreet gravida venenatis. Donec sit amet leo a eros fermentum fermentum. Quisque luctus purus massa, eget sollicitudin nulla mollis vitae. Etiam et pharetra dui, quis sagittis urna.",
    price: 84990,
    image:
      "https://media.croma.com/image/upload/v1606585943/Croma%20Assets/Computers%20Peripherals/Laptop/Images/9009480892446.png",
  },
  {
    id: 5,
    name: "Nike Men's Hypervenom 3 Club ",
    type: "sports",
    detail:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam tristique varius tincidunt. Sed faucibus accumsan lacus. Ut elementum vel ligula sed euismod. Duis laoreet gravida venenatis. Donec sit amet leo a eros fermentum fermentum. Quisque luctus purus massa, eget sollicitudin nulla mollis vitae. Etiam et pharetra dui, quis sagittis urna.",
    price: 3999,
    image:
      "https://statics.sportskeeda.com/wp-content/uploads/2014/09/62305-1410585744.jpg",
  },
  {
    id: 6,
    name: "KOOKABURRA English Willow Adult Cricket Bat KB Kahuna 800 No.5",
    type: "sports",
    detail:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam tristique varius tincidunt. Sed faucibus accumsan lacus. Ut elementum vel ligula sed euismod. Duis laoreet gravida venenatis. Donec sit amet leo a eros fermentum fermentum. Quisque luctus purus massa, eget sollicitudin nulla mollis vitae. Etiam et pharetra dui, quis sagittis urna.",
    price: 7700,
    image:
      "https://images-na.ssl-images-amazon.com/images/I/619YULn%2BVqL._SL1500_.jpg",
  },
];
