import SignIn from "./Pages/SignIn";
import "./App.css";

import { Route } from "react-router-dom";
import { Router } from "react-router-dom";
import history from "./Components/history";
import HomePage from "./Pages/HomePage";
import { Cart } from "./Pages/Cart";
import Sports1 from "./Pages/Sports1";
import Electronics1 from "./Pages/Electronics1";
import Books1 from "./Pages/Books1";

function App() {
  return (
    <>
      <Router history={history}>
        <Route exact path="/login" component={SignIn} />
        <Route exact path="/home" component={HomePage} />
        <Route exact path="/" component={SignIn} />
        <Route exact path="/cart" component={Cart} />
        <Route exact path="/sports" component={Sports1} />
        <Route exact path="/electronics" component={Electronics1} />
        <Route exact path="/books" component={Books1} />
      </Router>
    </>
  );
}

export default App;
