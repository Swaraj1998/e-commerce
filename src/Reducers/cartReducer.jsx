const initialState = { list: [] };

export default function cartReducer(state = initialState, action) {
  switch (action.type) {
    case "ADD_TO_CART":
      return {
        ...state,
        list: [...state.list, action.payload],
      };
    case "REMOVE_FROM_CART":
      const newProducts = state.list.filter(
        (currentData) => currentData.id !== action.payload.id
      );

      return {
        ...state,
        list: newProducts,
      };
    case "EMPTY_CART":
      return {
        ...state,
        list: [],
      };

    default:
      return state;
  }
}
