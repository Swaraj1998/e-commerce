import { combineReducers } from "redux";
import mainReducer from "./mainReducer";
import cartReducer from "./cartReducer";

const rootReducer = combineReducers({
  mainReducer,
  cartReducer,
});

export default rootReducer;
