const initialState = { list: [] };

export default function mainReducer(state = initialState, action) {
  switch (action.type) {
    case "DATA":
      return {
        ...state,
        list: [...state.list, action.payload],
      };

    case "LOGOUT_RESET":
      return { list: [] };

    default:
      return state;
  }
}
